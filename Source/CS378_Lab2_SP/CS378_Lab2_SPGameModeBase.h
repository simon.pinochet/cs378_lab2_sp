// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CS378_Lab2_SPGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB2_SP_API ACS378_Lab2_SPGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
