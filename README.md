Team: Group 14 

Branching scheme: Create a branch for every new features. We each have our own main branches that we don't delete. For every new feature we create a branch. We have 2 designated days in which we merge our code with master. Once the CI/CD build is successful on each merge to master, the branch for the feature is deleted.
